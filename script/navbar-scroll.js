$(function (params) {
    $(document).scroll(function (params) {
        var $nav = $(".navbar-fixed-top");
        $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
    })
})